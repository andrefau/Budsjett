import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-grid',
	templateUrl: './grid.component.html',
	styleUrls: ['./grid.component.css']
})
export class GridComponent implements OnInit {

	constructor() { }

	ngOnInit() {
	}
	
	data: any = [
		{
			"Post": "Lønn",
			"Andreas": 25000,
			"Mary": 0
		},
		{
			"Post": "Barnetrygd",
			"Andreas": 0,
			"Mary": 900
		}
	];

	columns: any = [
		"Post",
		{
			dataField: "Andreas",
			dataType: "number",
			format: "currency"
		},
		{
			dataField: "Mary",
			dataType: "number",
			format: "currency"
		},
	];

	editing: any = {
		mode: "row",
		allowUpdating: true,
		allowDeleting: true,
		allowAdding: true
	};

	summary: any = {
		totalItems: [{
			column: "Andreas",
			summaryType: "sum",
			valueFormat: "currency"
		}, {
			column: "Mary",
			summaryType: "sum",
			valueFormat: "currency"
		}]
	};

}
