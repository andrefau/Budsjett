import { BudsjettPage } from './app.po';

describe('budsjett App', function() {
  let page: BudsjettPage;

  beforeEach(() => {
    page = new BudsjettPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
